console.log("coucou");
const readline = require('readline'); // import du module readline

const rl = readline.createInterface({ // permet de pouvoir créer une interaction à travers le terminal
    input: process.stdin,
    output: process.stdout
});

// permet de choisir la fonction à lancer en fonction du besoin de l'utilisateur
rl.question(`tapez : "1" pour convertir de l'argent, \n "2" pour calculer les frais d'envoie d'un colis depuis la France,\n:`, function (choix: string) {
    if (choix === "1") {
        calculateurDevise()
    } else if (choix === "2") {
        fraisLivraison()
    }
})


function calculateurDevise() {
    // Demande du montant à changer
    rl.question('Entrez un montant :', function (montant: number) {
        console.log(`Vous avez saisi ${montant}`);

        if (montant > 0) { // vérification que le montant saisi soit supérieur à 0

            rl.question('Entrez la devise (EUR, CAD, JPY) :', function (devise: String) {
                rl.question('Tapez la devise dans laquelle vous voulez faire le change (EUR, CAD, JPY):', function (deviseOut: String) {
                    let total = 0;

                    function message() {
                        //message si la conversion a pu être éffectuée
                        console.log(`Vous souhaitez échanger ${montant}${devise} en ${deviseOut}. Cela donne : ${total}`)
                    }

                    function errorDeviseOut() {
                        //message si la conversion a rencontré un problème lié à une mauvaise saisi de la devise de change
                        console.log('Avez vous correctement saisi la devise de change ?')
                    }

                    // calcul convertion 
                    switch (devise) {
                        case "EUR":
                            //condition si deviseOut = CAD ou JPY etc
                            if (deviseOut === "CAD") {
                                //calcul de convertion
                                total = montant * 1.5
                                message() // message de la fonction message()
                                rl.close();
                            } else if (deviseOut === "JPY") {
                                //calcul de convertion
                                total = montant * 130
                                message()
                                rl.close();
                            } else if (deviseOut === "EUR") {
                                total = montant;
                                message()
                                rl.close();
                            } else {
                                //si ça rentre dans aucune contidition car chaine de caractère ne figurant dans aucune des conditions au dessus
                                errorDeviseOut() // message de la fonction errorDeviseOut()
                                rl.close();
                            }
                            break;
                        case "CAD":
                            if (deviseOut === "EUR") {
                                //calcul de convertion
                                total = montant / 1.5
                                message()
                                rl.close();
                            } else if (deviseOut === "JPY") {
                                //calcul de convertion
                                total = montant * 87
                                message()
                                rl.close();
                            } else if (deviseOut === "CAD") {
                                total = montant;
                                message()
                                rl.close();
                            } else {
                                errorDeviseOut()
                                rl.close();
                            }
                            break;
                        case "JPY":
                            if (deviseOut === "EUR") {
                                //calcul de convertion
                                total = montant / 130
                                message()
                                rl.close();
                            } else if (deviseOut === "CAD") {
                                //calcul de convertion
                                total = montant / 87
                                message()
                                rl.close();
                            } else if (deviseOut === "JPY") {
                                total = montant;
                                message()
                                rl.close();
                            } else {
                                errorDeviseOut()
                                rl.close();
                            }
                            break;
                        default:
                            console.log('Avez vous correctement saisi la devise actuelle ?');
                    }
                })
            })

        } else {
            console.log('Le montant saisi doit être positif et non nul.')
            rl.close();
        }

    });

}


function sommeDimensions(longueur: number, hauteur: number, largeur: number) {

    let total = longueur + hauteur + largeur
    if (isNaN(total)) {
        return `Vous ne devez pas entrer de caractères spéciaux ou de lettres.`;
    } else {
        return `la somme des dimmensions est : ${total}`;
    }
}

function fraisLivraison(): number { // précise le type de ce que j'aurai en sortie de la fonction

    let frais = 0;

    rl.question('Entrez la longueur du colis en cm :', function (longueur: string) {
        const longueur1 = +longueur // "+" pour convertir en type number
        rl.question('Entrez la hauteur du colis en cm :', function (hauteur: string) {
            const hauteur1 = +hauteur
            rl.question('Entrez la largeur du colis en cm :', function (largeur: string) {
                const largeur1 = +largeur

                let totalDimmension = sommeDimensions(longueur1, hauteur1, largeur1); // j'appelle la fonction sommeDimensions pour faire le total des longueurs

                console.log(totalDimmension)
                // console.log(typeof totalDimmension)

                rl.question('Entrez le pays de destination (france, canada, japon):', function (pays: string) {
                    rl.question('Entrez le poids du colis en Kg ( exemple : 1.5, 0.5 ... ):', function (poids: number) {

                        switch (pays) { // en fonction de la chaine de caractères saisie on rentre dans une case ou une autre
                            case "france":
                                if (poids > 0 && poids <= 1) { // condition pour appliquer le tarif en fonction du poids rensiegné
                                    frais = 10

                                    if (totalDimmension > "150") { // appliquer le tarif supplémentaire si les dimensions dépassent 150cm
                                        console.log(`les frais sont de ${frais + 5} EUR`)
                                        frais += 5
                                    } else {
                                        console.log(`les frais sont de ${frais} EUR`)
                                        frais
                                    }
                                } else if (poids > 1 && poids <= 3) {
                                    frais = 20

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 5} EUR`)
                                        frais += 5
                                    } else {
                                        console.log(`les frais sont de ${frais} EUR`)
                                        frais
                                    }
                                } else {
                                    frais = 30
                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 5} EUR`)
                                        frais += 5
                                    } else {
                                        console.log(`les frais sont de ${frais} EUR`)
                                        frais
                                    }
                                }
                                break;
                            case "canada":
                                if (poids > 0 && poids <= 1) {
                                    frais = 15
                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 7.5} CAD`)
                                        frais += 7.5
                                    } else {
                                        console.log(`les frais sont de ${frais} CAD`)
                                        frais
                                    }
                                } else if (poids > 1 && poids <= 3) {
                                    frais = 30

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 7.5} CAD`)
                                        frais += 7.5
                                    } else {
                                        console.log(`les frais sont de ${frais} CAD`)
                                        frais
                                    }
                                } else {
                                    frais = 45

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 7.5} CAD`)
                                        frais += 7.5
                                    } else {
                                        console.log(`les frais sont de ${frais} CAD`)
                                    }
                                }
                                break;
                            case "japon":
                                if (poids > 0 && poids <= 1) {
                                    frais = 1000

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 500} JPY`)
                                        frais += 500
                                    } else {
                                        console.log(`les frais sont de ${frais}JPY `)
                                    }
                                } else if (poids > 1 && poids <= 3) {
                                    frais = 2000

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 500} JPY`)
                                        frais += 500
                                    } else {
                                        console.log(`les frais sont de ${frais} JPY`)
                                    }
                                } else {
                                    frais = 3000

                                    if (totalDimmension > "150") {
                                        console.log(`les frais sont de ${frais + 500} JPY`)
                                        frais += 500
                                    } else {
                                        console.log(`les frais sont de ${frais} JPYs`)
                                    }
                                }
                                break;
                            default: // dans le cas ou la chaine de caracteres ne correspond à aucune case
                                console.log("Vous avez probablement mal renseigné un champs. Fermez puis recommencez.")
                                rl.close() // ferme l'interaction
                                return // arrête la fonction pour ne pas poursuivre le programme
                        }

                        //propose à l'utilisateur de continuer pour calculer les frais des douane
                        rl.question(`Voulez-vous calculer les frais de douane ? \n tapez : \n "1" pour OUI \n "2" pour NON\n:`, function (choix: string) {
                            if (choix === "1") {
                                fraisDouane(resultatFraisLivraison) // appelle la fonction fraisDouane et lui passe en paramètres ce qui est récupéré de la fonction fraisLivraison
                            } else if (choix === "2") {
                                rl.close() // quitte
                            } else {
                                console.log("Nous n'avons pas compris votre demande, vous devrez recommencer depuis le début lol")
                                rl.close()
                            }
                        })
                    })
                })
            })
        })
    })
    return frais // permet de faire sortir le résulat de la fonction pour le récupérer
}

let resultatFraisLivraison = fraisLivraison(); // récupère le résultat de la fonction fraisLivraison

function fraisDouane(resultatFraisLivraison: number): string {

    rl.question('Pouvez-vous réécrire le pays de destination ? (france, japon, canada ):', function (destination: string) {

        switch (destination) {// agir en fonction du pays renseigné
            case "france":
                console.log("Pas de frais supplémentaire pour la destination France.")
                rl.close()
                break;
            case "japon":
                if (resultatFraisLivraison > 20) {
                    resultatFraisLivraison *= 1.1
                    console.log(`Le coût total avec les frais de douanes est de ${resultatFraisLivraison}`)
                    rl.close()
                }
                break;
            case "canada":
                if (resultatFraisLivraison > 20) {
                    resultatFraisLivraison *= 1.15
                    console.log(`Le coût total avec les frais de douanes est de ${resultatFraisLivraison}`)
                    rl.close()
                }
                break;
            default: // dans le cas ou la chaine de caracteres ne correspond à aucune case
                console.log("Vous avez probablement mal renseigné un champs. Fermez puis recommencez... Depuis le début.")
                rl.close() // ferme l'interaction
                return // arrête la fonction pour ne pas poursuivre
        }
    })
    
    return `Le coût total avec les frais de douanes est de ${resultatFraisLivraison}`
}
