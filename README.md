# Faire un projet Tyspescript avec Node

### créer fichier typescript

Créer un dossier, créer un fichier nomDuFichier.ts
En général on crée dans le projet un dossier "src" dans lequel on viendra ranger notre fichier .ts

### générer le fichier package.json

Faire `npm init -y` pour générer le fichier **package.json**

### installer ts 

Taper commande : `npm i --save-dev typescript @types/node`
Puis : `npx tsc --init`

### création d'un fichier .js qui contiendra le même code que le fichier .ts

Taper commande : `npx tsc src/nomDuFichier.ts --outDir dist`

( src -> nom de dossier pour mon cas )

### compiler manuellement 

À chaque nouvelle sauvegarde de fichier, si les modifications ne sont pas compilées on ne peut pas voir de changement.

Ajouter :
`"build": "tsc",` 
dans la partie script du fichier **package.json**

Dans **tsconfig.json**, dans la partie **Emit**, trouver et décommenter la ligne **outDir** et ajouter après le **./** le nom du dossier où se trouve votre fichier js ( pour mon cas ça donne `"outDir": "./dist",`)

Taper commande : npm run build

Pour voir le résultat, taper commande : `node dist` ( puisque dans mon cas j'ai créé un dossier nommé dist dans lequel sera créé le fichier js)

### comment fonctionne mon application

Elle repose entièrement sur une intéraction à travers le terminal grâce aux commandes readline.
Lancez la commande `node dist` et laissez vous guider.

pour en sortir en milieu du programme faire la commande :
pour linux : control + c  
